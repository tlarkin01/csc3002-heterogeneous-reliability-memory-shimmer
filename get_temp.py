#!/usr/bin/python
import sys
import numpy as np
import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt

from matplotlib import cm
import itertools
import os
from matplotlib import cm
from scipy.interpolate import pchip
import matplotlib.colors as colors

import matplotlib as mpl

from scipy.interpolate import pchip


import scipy as sp
import scipy.stats

bench_temp={}

mcu_list=['MCU0','MCU1','MCU2','MCU3']

for mcu in mcu_list:
    bench_temp[mcu]=[]

stat_info = open(sys.argv[1], "r")
prev_line=''
for line in stat_info:
       if line.find("0x") == 0:
         if prev_line.find("CH0 DIMM Temp") >= 0:
            bench_temp['MCU0'].append(int(line.rstrip(),16))
         if prev_line.find("CH1 DIMM Temp") >= 0:
            bench_temp['MCU1'].append(int(line.rstrip(),16))
         if prev_line.find("CH2 DIMM Temp") >= 0:
            bench_temp['MCU2'].append(int(line.rstrip(),16))
         if prev_line.find("CH3 DIMM Temp") >= 0:
            bench_temp['MCU3'].append(int(line.rstrip(),16))
       prev_line=line
stat_info.close()

for mcu in mcu_list:
  print mcu
  print np.mean(bench_temp[mcu])
