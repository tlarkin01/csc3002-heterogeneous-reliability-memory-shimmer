
#!/bin/bash
inst_id="QUB"
start_date=$(date +"%m%d%Y_%H%M%S")
report_name_msr=governor_msr_report_$start_date\_$1
I2CDEV=1
while true
do
         echo "-----------" >> $report_name_msr
         date >> $report_name_msr
         echo "SOC Temp" >> $report_name_msr
         i2cget -y $I2CDEV 0x2f 0x10 w >> $report_name_msr
         echo "SOC VRD Temp" >> $report_name_msr
         i2cget -y $I2CDEV 0x2f 0x11 w >> $report_name_msr
         echo "DIMM VRD Temp" >> $report_name_msr
         i2cget -y $I2CDEV 0x2f 0x12 w >> $report_name_msr

         echo "CH0 DIMM Temp" >> $report_name_msr
         i2cget -y $I2CDEV 0x2f 0x14 w >> $report_name_msr
         echo "CH1 DIMM Temp" >> $report_name_msr
         i2cget -y $I2CDEV 0x2f 0x15 w >> $report_name_msr
         echo "CH2 DIMM Temp" >> $report_name_msr
         i2cget -y $I2CDEV 0x2f 0x16 w >> $report_name_msr
         echo "CH3 DIMM Temp" >> $report_name_msr
         i2cget -y $I2CDEV 0x2f 0x17 w >> $report_name_msr
         echo "CH4 DIMM Temp" >> $report_name_msr
         i2cget -y $I2CDEV 0x2f 0x18 w >> $report_name_msr
         echo "CH5 DIMM Temp" >> $report_name_msr
         i2cget -y $I2CDEV 0x2f 0x19 w >> $report_name_msr
         echo "CH6 DIMM Temp" >> $report_name_msr
         i2cget -y $I2CDEV 0x2f 0x1a w >> $report_name_msr
         echo "CH7 DIMM Temp" >> $report_name_msr
         i2cget -y $I2CDEV 0x2f 0x1b w >> $report_name_msr

         echo "DIMM VRD1 Pow" >> $report_name_msr
         i2cget -y $I2CDEV 0x2f 0x24 w >> $report_name_msr
         echo "DIMM VRD1 Pow mw" >> $report_name_msr
         i2cget -y $I2CDEV 0x2f 0x25 w >> $report_name_msr
         echo "DIMM VRD2 Pow" >> $report_name_msr
         i2cget -y $I2CDEV 0x2f 0x26 w >> $report_name_msr
         echo "DIMM VRD2 Pow mw" >> $report_name_msr
         i2cget -y $I2CDEV 0x2f 0x27 w >> $report_name_msr

         echo "PMD VRD Pow" >> $report_name_msr
         i2cget -y $I2CDEV 0x2f 0x20 w >> $report_name_msr
         echo "PMD VRD Pow mw" >> $report_name_msr
         i2cget -y $I2CDEV 0x2f 0x21 w >> $report_name_msr
         echo "SOC VRD Pow" >> $report_name_msr
         i2cget -y $I2CDEV 0x2f 0x22 w >> $report_name_msr
         echo "SOC VRD Pow mw" >> $report_name_msr
         i2cget -y $I2CDEV 0x2f 0x23 w >> $report_name_msr
         sleep 1
 done

 exit 0
