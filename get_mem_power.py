#!/usr/bin/python

import sys
import os
import random
import numpy as np
import subprocess

def func():
     statfile = open(sys.argv[1], "r")
     res_run1=0

     pow_list=[]
     pow_list2=[]
     pow=0
     pow_mw=0
     prev_line=''
     for line in statfile:
         if prev_line.find("DIMM VRD1 Pow mw") >= 0:
            pow_mw=int(line, 16)
#            if pow > 5:
            pow_list.append(float(pow)+float(pow_mw)/1000.0)
         if prev_line.find("DIMM VRD1 Pow") >= 0:
            pow=int(line, 16)

         prev_line=line

     print "----------------"
     pow_list1=pow_list

     res_run1=np.mean(pow_list)
     statfile.close()

     statfile = open(sys.argv[1], "r")
     res_run2=0

     pow_list=[]
     pow=0
     pow_mw=0
     prev_line=''
     for line in statfile:
         if prev_line.find("DIMM VRD2 Pow mw") >= 0:
            pow_mw=int(line, 16)
#            if pow > 5:
            pow_list.append(float(pow)+float(pow_mw)/1000.0)
         if prev_line.find("DIMM VRD2 Pow") >= 0:
            pow=int(line, 16)

         prev_line=line

     pow_list2=pow_list
     res_run2=np.mean(pow_list)

#     for i in range(0,min(len(pow_list1),len(pow_list2))):
#       print pow_list1[i]+pow_list2[i]

     print ">>>>>>>>"
     print res_run1+res_run2


func()

