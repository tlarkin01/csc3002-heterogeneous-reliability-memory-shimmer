Hi George / Assessor

Here you can find all the scripts I developed acccompanied with the graphs I have plotted in relation to the project.

You can find a history of regular commits and all of my work on HPDC02, the link is available under the second reference in my thesis. 

To use the scripts: (I advise running these on HPDC02 under /home/tlarkin01/NPB3.3.1/NPB3.3-OMP)
1) Run script.sh (This will also run measure_power.sh)
2) Upon completion of script running, two reports will be generated 'trace_application_bt' & 'governor_msr_reportDDMMYY'
3) Trace_application can be read directly with the human eye
4) 'governor_msr_reportDDMMYY' - needs to have the relevant data collected by the three python scripts. e.g get_cpu_power'
5) To run: ./get_cpu_power.py <location of governor_msr_reportDDMMYY>   (This can be done for each of the python scripts)


Kind Regards
Thomas Larkin