#!/bin/bash

benchmark='bt cg ep lu mg sp ua'
#no dc or is for class D
#insert1

DRAM_REF=0xadf

DRAM_VOLT=0x594
I2CDEV=1

for govrn in $(ls /sys/devices/system/cpu/cpu*/cpufreq/scaling_governor) ; do

    echo "userspace" | tee $govrn

    sync

done


for freq in $(ls /sys/devices/system/cpu/cpu*/cpufreq/scaling_setspeed) ; do

    echo "2400000" | tee $freq

    sync

done


for freq in $(ls /sys/devices/system/cpu/cpu*/cpufreq/scaling_min_freq) ; do

    echo "2400000" | tee $freq

    sync

done




i2cset -y $I2CDEV 0x2f 0x36 $DRAM_VOLT w

sync

i2cset -y $I2CDEV 0x2f 0x37 $DRAM_VOLT w

sync

i2cset -y $I2CDEV 0x2f 0xf2 0x0000 w

sync

i2cset -y $I2CDEV 0x2f 0xf3 $DRAM_REF w

sync


i2cset -y $I2CDEV 0x2f 0xf2 0x0001 w

sync

i2cset -y $I2CDEV 0x2f 0xf3 $DRAM_REF w

sync



i2cset -y $I2CDEV 0x2f 0xf2 0x0002 w

sync

i2cset -y $I2CDEV 0x2f 0xf3 $DRAM_REF w
sync


i2cset -y $I2CDEV 0x2f 0xf2 0x0003 w

sync

i2cset -y $I2CDEV 0x2f 0xf3 $DRAM_REF w

sync



for benchmark in $benchmark
do

        echo $benchmark

        make clean

        echo "before each run"
        sudo echo 200000        > sudo tee /sys/kernel/debug/tracing/buffer_size_kb
        sudo echo ghes_print_status > sudo tee /sys/kernel/debug/tracing/set_ftrace_filter
        sudo echo function      > sudo tee /sys/kernel/debug/tracing/current_tracer
        sudo echo "" | sudo tee /sys/kernel/debug/tracing/trace

        dmesg &> dmesg_application_orig

        make $benchmark CLASS=D
        cd bin

        end=$((SECONDS+3600))           #3600s is one hour
        sudo ./measure_power.sh $benchmark_D &

        while [ $SECONDS -lt $end ]; do
        timeout 2h numactl --cpunodebind=0 --interleave=1,2 ./$benchmark.D.x
        :
        done

        sudo pkill measure_power.sh

        echo "Writing to trace_application_"$benchmark
        sudo cp /sys/kernel/debug/tracing/current_tracer trace_application_$benchmark
        dmesg &> dmesg_application_tmp
        diff dmesg_application_tmp dmesg_application_orig > dmesg_application_$benchmark

        sudo echo function > /sys/kernel/debug/tracing/current_tracer
        cd ..

        make clean


done


#insert2

DRAM_REF=0x4c

DRAM_VOLT=0x5dc


for govrn in $(ls /sys/devices/system/cpu/cpu*/cpufreq/scaling_governor) ; do

    echo "userspace" | tee $govrn

    sync

done


for freq in $(ls /sys/devices/system/cpu/cpu*/cpufreq/scaling_setspeed) ; do

    echo "2400000" | tee $freq

    sync

done


for freq in $(ls /sys/devices/system/cpu/cpu*/cpufreq/scaling_min_freq) ; do

    echo "2400000" | tee $freq

    sync

done




i2cset -y $I2CDEV 0x2f 0x36 $DRAM_VOLT w

sync

i2cset -y $I2CDEV 0x2f 0x37 $DRAM_VOLT w

sync





i2cset -y $I2CDEV 0x2f 0xf2 0x0000 w

sync

i2cset -y $I2CDEV 0x2f 0xf3 $DRAM_REF w

sync


i2cset -y $I2CDEV 0x2f 0xf2 0x0001 w

sync

i2cset -y $I2CDEV 0x2f 0xf3 $DRAM_REF w

sync



i2cset -y $I2CDEV 0x2f 0xf2 0x0002 w

sync

i2cset -y $I2CDEV 0x2f 0xf3 $DRAM_REF w


sync


i2cset -y $I2CDEV 0x2f 0xf2 0x0003 w

sync

i2cset -y $I2CDEV 0x2f 0xf3 $DRAM_REF w

sync

